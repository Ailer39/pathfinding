﻿using PathFinding.Enviroment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding.Algorithm.Algorithms
{
    public class Dijkstra : PathFindingBase
    {
        #region - private

        private double[,] _weights;

        #region - functions

        private void InitializeWeights(IField startField)
        {
            this._weights = new double[this.Board.GetXSize(), this.Board.GetYSize()];

            for (int y = 0; y < this.Board.GetYSize(); y++)
            {
                for (int x = 0; x < this.Board.GetXSize(); x++)
                {
                    this._weights[x, y] = int.MaxValue;
                }
           } 

            this._weights[startField.X, startField.Y] = 0;
            //startField.FieldCost = 0;
        }

        #endregion
        #endregion

        #region - public
        #region - functions

        public async override Task<Stack<IField>> GetPath(IField startField, IField targetField)
        {
            base.AncestorList.Clear();
            Task<Stack<IField>> t = new Task<Stack<IField>>(() =>
            {
                IDictionary<int, IField> closedList = new Dictionary<int, IField>();
                IDictionary<int, IField> visitedFields = new Dictionary<int, IField>();
                IField currentField;
                IField tmpField;
                double weight;

                visitedFields.Add(startField.Id, startField);
                InitializeWeights(startField);

                while (!visitedFields.ContainsKey(targetField.Id))
                {
                    if (visitedFields.Count == 0)
                    {
                        break;
                    }

                    currentField = visitedFields.OrderBy(o => _weights[o.Value.X, o.Value.Y]).First().Value;
                    foreach (IField neighbour in this.Board.GetFieldNeighbours(currentField.Id))
                    {
                        if (!closedList.ContainsKey(neighbour.Id) &&
                            !Board.IsWall(neighbour.Id))
                        {
                            weight = CalculatePathCost(_weights[currentField.X, currentField.Y], currentField, neighbour);

                            if (visitedFields.ContainsKey(neighbour.Id))
                            {
                                tmpField = visitedFields[neighbour.Id];                            

                                if (_weights[tmpField.X, tmpField.Y] > weight)
                                {
                                    _weights[tmpField.X, tmpField.Y] = weight;
                                    base.SetAncetor(neighbour.Id, currentField.Id);
                                }
                            }
                            else
                            {
                                _weights[neighbour.X, neighbour.Y] = weight;
                                base.SetAncetor(neighbour.Id, currentField.Id);
                                visitedFields.Add(neighbour.Id, neighbour);
                            }
                        }
                    }

                    visitedFields.Remove(currentField.Id);
                    closedList.Add(currentField.Id, currentField);
                }

                return CreatePath(startField.Id, targetField.Id);
            });

            t.Start();
            return await t;
        }
        #endregion

        #region - ctor

        public Dijkstra(IBoard board)
             : base("Dijkstra", board)
        { }
        #endregion
        #endregion
    }
}

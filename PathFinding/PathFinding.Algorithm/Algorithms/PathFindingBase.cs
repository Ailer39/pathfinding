﻿using PathFinding.Enviroment;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PathFinding.Algorithm.Algorithms
{
    public abstract class PathFindingBase
    {
        #region - private

        private string _algorithmName;

        #endregion

        #region - protected
        #region - properties

        protected IBoard Board { get; private set; }

        protected IDictionary<int, int> AncestorList { get; private set; }

        #endregion

        #region - functions
        protected virtual Stack<IField> CreatePath(int startFieldId, int targetFieldId)
        {
            Stack<IField> result = new Stack<IField>();
            int ancestor = targetFieldId;
            IField field;
            PathLength = 0;
            PathCost = 0;

            if (!AncestorList.ContainsKey(targetFieldId))
            {
                throw new Exception("Target field not found.");
            }

            do
            {
                field = Board.GetField(ancestor);
                result.Push(field);
                ancestor = AncestorList[ancestor];
                PathLength += 1;
                PathCost = CalculatePathCost(PathCost, Board.GetField(ancestor), field);
            }
            while (ancestor != startFieldId);

            return result;
        }

        protected double CalculatePathCost(double currentPathCost, IField startField, IField targetField)
        {
            double fieldCost = targetField.FieldCost;

            if (Board.IsDiagonalNeighbour(startField, targetField))
            {
                fieldCost *= Math.Sqrt(2);
            }

            return currentPathCost + fieldCost;
        }
        #endregion
        #endregion

        #region - public
        #region - properties
        public double PathCost { get; protected set; }
        public int PathLength { get; protected set; }
        #endregion

        #region - functions

        public abstract Task<Stack<IField>> GetPath(IField startField, IField targetField);

        public override string ToString()
        {
            return _algorithmName;
        }

        public void UpdateBoard(IBoard board)
        {
            Board = board;
        }

        public virtual void SetAncetor(int fieldId, int ancestorFieldId)
        {
            if (AncestorList.ContainsKey(fieldId))
            {
                AncestorList[fieldId] = ancestorFieldId;
            }
            else
            {
                AncestorList.Add(fieldId, ancestorFieldId);
            }
        }
        #endregion

        #region - ctors

        public PathFindingBase(String algorithmName, IBoard board)
        {
            this.Board = board;
            _algorithmName = algorithmName;
            AncestorList = new Dictionary<int, int>();
        }
        #endregion
        #endregion
    }
}

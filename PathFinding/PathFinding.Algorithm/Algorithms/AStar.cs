﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PathFinding.Enviroment;
using PathFinding.Algorithm.Heuristic;

namespace PathFinding.Algorithm.Algorithms
{
    public class AStar : PathFindingBase
    {
        #region - private 
        #region - vars

        private IDictionary<int, double> _distancesToTarget;
        private IHeuristicCalculator _heuristicCalculator;

        #endregion

        #region - functions
        private void CalculateDistances(IField targetField)
        {
            _distancesToTarget = new Dictionary<int, double>();
            int distanceX;
            int distanceY;

            for (int y = 0; y < Board.GetYSize(); y++)
            {
                for (int x = 0; x < Board.GetXSize(); x++)
                {
                    if (!(targetField.X == x &&
                          targetField.Y == y))
                    {
                        _distancesToTarget.Add(Board.GetField(x, y).Id,
                                                _heuristicCalculator.Calculate(x, y, targetField.X, targetField.Y));
                    }
                    else
                    {
                        _distancesToTarget.Add(targetField.Id, 0);
                    }
                }
            }
        }
        #endregion
        #endregion

        #region - public
        #region - functions
        public override Task<Stack<IField>> GetPath(IField startField, IField targetField)
        {
            base.AncestorList.Clear();

            Task<Stack<IField>> t = new Task<Stack<IField>>(() =>
            {
                IDictionary<int, IField> closedList = new Dictionary<int, IField>();
                IDictionary<int, double> visitedFields = new Dictionary<int, double>();
                IField currentField = null;
                double pathCost = 0;
                CalculateDistances(targetField);
                visitedFields.Add(startField.Id, 0);

                while (!closedList.ContainsKey(targetField.Id))
                {
                    if (visitedFields.Count == 0)
                    {
                        break;
                    }

                    currentField = Board.GetField(visitedFields.OrderBy(o => _distancesToTarget[o.Key] + Board.GetField(o.Key).FieldCost)
                                         .First()
                                         .Key);

                    foreach (IField neighbour in Board.GetFieldNeighbours(currentField.Id))
                    {
                        if (!closedList.ContainsKey(neighbour.Id) &&
                            !Board.IsWall(neighbour.Id))
                        {
                            pathCost = CalculatePathCost(visitedFields[currentField.Id], currentField, targetField);

                            if (visitedFields.ContainsKey(neighbour.Id))
                            {
                                if (visitedFields[neighbour.Id] > pathCost)
                                {
                                    visitedFields[neighbour.Id] = pathCost;
                                    base.SetAncetor(neighbour.Id, currentField.Id);
                                }
                            }
                            else
                            {
                                visitedFields.Add(neighbour.Id, pathCost);
                                base.SetAncetor(neighbour.Id, currentField.Id);
                            }
                        }
                    }

                    closedList.Add(currentField.Id, currentField);
                    visitedFields.Remove(currentField.Id);
                }

                return CreatePath(startField.Id, targetField.Id);
            });

            t.Start();
            return t;
        }
        #endregion

        #region - ctors

        public AStar(IBoard board, IHeuristicCalculator heuristic)
            : base(string.Format("A* {0}", heuristic.GetName()), board)
        {
            _heuristicCalculator = heuristic;
        }
        #endregion
        #endregion
    }
}

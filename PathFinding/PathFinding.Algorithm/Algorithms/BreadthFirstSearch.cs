﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PathFinding.Enviroment;

namespace PathFinding.Algorithm.Algorithms
{
    public class BreadthFirstSearch : PathFindingBase
    {
        #region - private
        #endregion

        #region - public
        #region - functions
        #endregion

        #region - ctors

        public BreadthFirstSearch(IBoard board)
            : base("Breadth First Search", board)
        { }
        #endregion
        #endregion

        public override Task<Stack<IField>> GetPath(IField startField, IField targetField)
        {
            Task<Stack<IField>> t = new Task<Stack<IField>>(() =>
            {
                IDictionary<int, IField> closedList = new Dictionary<int, IField>();
                IDictionary<int, double> pathCostList = new Dictionary<int, double>();
                Queue<int> openList = new Queue<int>();
                int currentFieldId;
                double pathCost;
                pathCostList.Add(startField.Id, 0);
                openList.Enqueue(startField.Id);
             

                while (!closedList.ContainsKey(targetField.Id))
                {
                    if (openList.Count == 0)
                    {
                        break;
                    }

                    currentFieldId = openList.Dequeue();

                    foreach (IField neighbour in Board.GetFieldNeighbours(currentFieldId))
                    {
                        if (!closedList.ContainsKey(neighbour.Id))
                        {
                            openList.Enqueue(neighbour.Id);
                            pathCost = CalculatePathCost(pathCostList[currentFieldId], Board.GetField(currentFieldId), neighbour);

                            if ((pathCostList.ContainsKey(neighbour.Id) &&
                                 pathCostList[neighbour.Id] < pathCost))
                            {
                                pathCostList[neighbour.Id] = pathCost;
                                SetAncetor(neighbour.Id, currentFieldId);
                            }
                            else if (!pathCostList.ContainsKey(neighbour.Id))
                            {
                                pathCostList.Add(neighbour.Id, currentFieldId);
                                SetAncetor(neighbour.Id, currentFieldId);
                            }

                            base.SetAncetor(neighbour.Id, currentFieldId);
                        }                        
                    }

                    if (!closedList.ContainsKey(currentFieldId))
                    {
                        closedList.Add(currentFieldId, Board.GetField(currentFieldId));
                    }
                }

                return CreatePath(startField.Id, targetField.Id);
            });
            t.Start();
            return t;
        }
    }
}

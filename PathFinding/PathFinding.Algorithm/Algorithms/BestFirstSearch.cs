﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PathFinding.Enviroment;
using PathFinding.Algorithm.Heuristic;

namespace PathFinding.Algorithm.Algorithms
{
    public class BestFirstSearch : PathFindingBase
    {
        #region - private 
        #region - vars

        private IDictionary<int, double> _distancesToTarget;
        private IHeuristicCalculator _heuristicCalculator;

        #endregion

        #region - functions
        private void CalculateDistances(IField targetField)
        {
            _distancesToTarget = new Dictionary<int, double>();

            for (int y = 0; y < Board.GetYSize(); y++)
            {
                for (int x = 0; x < Board.GetXSize(); x++)
                {
                    if (!(targetField.X == x &&
                        targetField.Y == y))
                    {
                        _distancesToTarget.Add(Board.GetField(x, y).Id, _heuristicCalculator.Calculate(x, y, targetField.X, targetField.Y));
                    }
                    else
                    {
                        _distancesToTarget.Add(targetField.Id, 0);
                    }
                }
            }
        }
        #endregion
        #endregion

        #region - public 

        #region - ctor

        public BestFirstSearch(IBoard board, IHeuristicCalculator heuristicCalculator)
            : base(string.Format("Best first Search* {0}", heuristicCalculator.GetName()), board)
        {
            _heuristicCalculator = heuristicCalculator;
        }
        #endregion

        #region - functions

        public override Task<Stack<IField>> GetPath(IField startField, IField targetField)
        {
            base.AncestorList.Clear();
            Task<Stack<IField>> t = new Task<Stack<IField>>(() =>
            {
                IDictionary<int, IField> closedList = new Dictionary<int, IField>();
                IField currentField = startField;
                closedList.Add(startField.Id, startField);
                CalculateDistances(targetField);

                while (!closedList.ContainsKey(targetField.Id))
                {
                    if (currentField == null)
                    {
                        break;
                    }

                    foreach (IField neighbour in Board.GetFieldNeighbours(currentField.Id)
                                                        .OrderBy(o => _distancesToTarget[o.Id]))
                    {
                        if (!closedList.ContainsKey(neighbour.Id) &&
                            !Board.IsWall(neighbour.Id))
                        {
                            base.SetAncetor(neighbour.Id, currentField.Id);
                            currentField = neighbour;
                            break;
                        }
                    }

                    if (!closedList.ContainsKey(currentField.Id))
                    {
                        closedList.Add(currentField.Id, currentField);
                    }
                }

                return CreatePath(startField.Id, targetField.Id);
            });

            t.Start();
            return t;
        }
        #endregion
        #endregion
    }
}
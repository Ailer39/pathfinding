﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding.Algorithm.Heuristic
{
    public class ManhattanHeuristicCalculator : IHeuristicCalculator
    {
        public double Calculate(int xField, int yField, int xTargetField, int yTargetField)
        {
            int distanceX = Math.Abs(xTargetField - xField);
            int distanceY = Math.Abs(yTargetField - yField);

            return distanceX + distanceY;
        }

        public string GetName()
        {
            return "Manhattan";
        }
    }
}

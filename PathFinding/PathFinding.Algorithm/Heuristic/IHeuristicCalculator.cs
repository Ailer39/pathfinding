﻿using PathFinding.Enviroment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding.Algorithm.Heuristic
{
    public interface IHeuristicCalculator
    {
        double Calculate(int xField, int yField, int xTargetField, int yTargetField);
        string GetName();
    }
}

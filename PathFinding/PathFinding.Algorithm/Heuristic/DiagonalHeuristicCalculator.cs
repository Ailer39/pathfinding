﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding.Algorithm.Heuristic
{
    public class DiagonalHeuristicCalculator : IHeuristicCalculator
    {
        public double Calculate(int xField, int yField, int xTargetField, int yTargetField)
        {
            int distanceX = Math.Abs(xTargetField - xField);
            int distanceY = Math.Abs(yTargetField - yField);

            return (distanceX + distanceY) + (Math.Sqrt(2) - 2 * 1) * Math.Min(distanceX, distanceY);
        }

        public string GetName()
        {
            return "Diagonal";
        }
    }
}

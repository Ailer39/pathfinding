﻿using System.Collections.Generic;

namespace PathFinding.Enviroment
{
    public interface IBoard
    {
        void AddWall(int x, int y);
        void CreateBoard();
        IField[,] GetBoard();
        IField GetField(int id);
        IField GetField(int x, int y);
        IList<IField> GetFieldNeighbours(int fieldId);
        int GetXSize();
        int GetYSize();
        bool IsDiagonalNeighbour(IField field, IField neighbour);
        bool IsWall(int id);
        bool IsWall(int x, int y);
        void RemoveWall(int x, int y);
    }
}
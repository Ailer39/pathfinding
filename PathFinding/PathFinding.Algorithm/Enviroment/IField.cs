﻿namespace PathFinding.Enviroment
{
    public interface IField
    {
        int FieldCost { get; set; }
        int Id { get; set; }
        int X { get; }
        int Y { get; }
    }
}
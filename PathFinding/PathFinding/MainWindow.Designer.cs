﻿namespace PathFinding
{
    partial class MainWindow
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.dGVBoard = new System.Windows.Forms.DataGridView();
            this.BtStart = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RbWall = new System.Windows.Forms.RadioButton();
            this.LaTime = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CbAlgorithm = new System.Windows.Forms.ComboBox();
            this.RbTarget = new System.Windows.Forms.RadioButton();
            this.RBStart = new System.Windows.Forms.RadioButton();
            this.LaPathLength = new System.Windows.Forms.Label();
            this.LaPathCost = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.PbProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dGVBoard)).BeginInit();
            this.panel2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dGVBoard
            // 
            this.dGVBoard.AllowUserToAddRows = false;
            this.dGVBoard.AllowUserToDeleteRows = false;
            this.dGVBoard.AllowUserToResizeColumns = false;
            this.dGVBoard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVBoard.ColumnHeadersVisible = false;
            this.dGVBoard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dGVBoard.Location = new System.Drawing.Point(3, 16);
            this.dGVBoard.Name = "dGVBoard";
            this.dGVBoard.ReadOnly = true;
            this.dGVBoard.Size = new System.Drawing.Size(553, 285);
            this.dGVBoard.TabIndex = 0;
            this.dGVBoard.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dGV_CellClick);
            // 
            // BtStart
            // 
            this.BtStart.Location = new System.Drawing.Point(95, 83);
            this.BtStart.Name = "BtStart";
            this.BtStart.Size = new System.Drawing.Size(83, 23);
            this.BtStart.TabIndex = 6;
            this.BtStart.Text = "Start";
            this.BtStart.UseVisualStyleBackColor = true;
            this.BtStart.Click += new System.EventHandler(this.BtStart_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.RbWall);
            this.panel2.Controls.Add(this.LaTime);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.CbAlgorithm);
            this.panel2.Controls.Add(this.RbTarget);
            this.panel2.Controls.Add(this.RBStart);
            this.panel2.Controls.Add(this.BtStart);
            this.panel2.Controls.Add(this.LaPathLength);
            this.panel2.Controls.Add(this.LaPathCost);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(559, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(190, 326);
            this.panel2.TabIndex = 2;
            // 
            // RbWall
            // 
            this.RbWall.AutoSize = true;
            this.RbWall.Location = new System.Drawing.Point(125, 12);
            this.RbWall.Name = "RbWall";
            this.RbWall.Size = new System.Drawing.Size(46, 17);
            this.RbWall.TabIndex = 13;
            this.RbWall.TabStop = true;
            this.RbWall.Text = "Wall";
            this.RbWall.UseVisualStyleBackColor = true;
            // 
            // LaTime
            // 
            this.LaTime.AutoSize = true;
            this.LaTime.Location = new System.Drawing.Point(98, 212);
            this.LaTime.Name = "LaTime";
            this.LaTime.Size = new System.Drawing.Size(13, 13);
            this.LaTime.TabIndex = 12;
            this.LaTime.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 212);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Execution time:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Algorithm:";
            // 
            // CbAlgorithm
            // 
            this.CbAlgorithm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbAlgorithm.FormattingEnabled = true;
            this.CbAlgorithm.Location = new System.Drawing.Point(62, 45);
            this.CbAlgorithm.Name = "CbAlgorithm";
            this.CbAlgorithm.Size = new System.Drawing.Size(116, 21);
            this.CbAlgorithm.TabIndex = 9;
            // 
            // RbTarget
            // 
            this.RbTarget.AutoSize = true;
            this.RbTarget.Location = new System.Drawing.Point(62, 12);
            this.RbTarget.Name = "RbTarget";
            this.RbTarget.Size = new System.Drawing.Size(56, 17);
            this.RbTarget.TabIndex = 8;
            this.RbTarget.Text = "Target";
            this.RbTarget.UseVisualStyleBackColor = true;
            // 
            // RBStart
            // 
            this.RBStart.AutoSize = true;
            this.RBStart.Checked = true;
            this.RBStart.Location = new System.Drawing.Point(9, 12);
            this.RBStart.Name = "RBStart";
            this.RBStart.Size = new System.Drawing.Size(47, 17);
            this.RBStart.TabIndex = 7;
            this.RBStart.TabStop = true;
            this.RBStart.Text = "Start";
            this.RBStart.UseVisualStyleBackColor = true;
            // 
            // LaPathLength
            // 
            this.LaPathLength.AutoSize = true;
            this.LaPathLength.Location = new System.Drawing.Point(98, 181);
            this.LaPathLength.Name = "LaPathLength";
            this.LaPathLength.Size = new System.Drawing.Size(13, 13);
            this.LaPathLength.TabIndex = 3;
            this.LaPathLength.Text = "0";
            // 
            // LaPathCost
            // 
            this.LaPathCost.AutoSize = true;
            this.LaPathCost.Location = new System.Drawing.Point(98, 145);
            this.LaPathCost.Name = "LaPathCost";
            this.LaPathCost.Size = new System.Drawing.Size(13, 13);
            this.LaPathCost.TabIndex = 2;
            this.LaPathCost.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 181);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Path length:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Path cost:";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PbProgress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 304);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(559, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // PbProgress
            // 
            this.PbProgress.Name = "PbProgress";
            this.PbProgress.Size = new System.Drawing.Size(100, 16);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dGVBoard);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(559, 304);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Board";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(749, 326);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel2);
            this.Name = "MainWindow";
            this.Text = "MainWindow";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dGVBoard)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dGVBoard;
        private System.Windows.Forms.Button BtStart;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label LaPathLength;
        private System.Windows.Forms.Label LaPathCost;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar PbProgress;
        private System.Windows.Forms.RadioButton RbTarget;
        private System.Windows.Forms.RadioButton RBStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox CbAlgorithm;
        private System.Windows.Forms.Label LaTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton RbWall;
    }
}


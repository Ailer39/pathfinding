﻿using PathFinding.Algorithm.Algorithms;
using PathFinding.Algorithm.Heuristic;
using PathFinding.Enviroment;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace PathFinding
{
    public partial class MainWindow : Form
    {
        #region - private
        #region - vars
        private Board _board;
        private IField _startField;
        private IField _targetField;

        private const int xSize = 10;
        private const int ySize = 10;

        private DataGridViewCellStyle _startCellStyle;
        private DataGridViewCellStyle _targetCellStyle;
        private DataGridViewCellStyle _pathCellStyle;
        private DataGridViewCellStyle _wallCellStyle;

        #endregion
        #endregion

        #region - public 
        #region - functions
        #region - eventhandler
        private void BtStart_Click(object sender, EventArgs e)
        {
            BtStart.Enabled = false;
            if (_startField != null &&
                _targetField != null)
            {
                PbProgress.Style = ProgressBarStyle.Marquee;
                ExecuteAlrogithm();
            }
            else
            {
                MessageBox.Show("No start and or target field");
            }
            BtStart.Enabled = true;
        }

        private void dGV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (RBStart.Checked)
            {
                if (_startField != null)
                {
                    ChangeCellStyle(_startField.X, _startField.Y, null);
                }

                RBStart.Checked = false;
                RbTarget.Checked = true;
                _startField = _board.GetField(e.ColumnIndex, e.RowIndex);
                ChangeCellStyle(_startField.X, _startField.Y, _startCellStyle);
            }
            else if (RbTarget.Checked)
            {
                if (_targetField != null)
                {
                    ChangeCellStyle(_targetField.X, _targetField.Y, null);
                }

                _targetField = _board.GetField(e.ColumnIndex, e.RowIndex);
                ChangeCellStyle(_targetField.X, _targetField.Y, _targetCellStyle);
                RBStart.Checked = true;
            }
            else if (RbWall.Checked)
            {
                if (_board.IsWall(e.ColumnIndex, e.RowIndex))
                {
                    _board.RemoveWall(e.ColumnIndex, e.RowIndex);
                    ChangeCellStyle(e.ColumnIndex, e.RowIndex, null);
                }
                else
                {
                    _board.AddWall(e.ColumnIndex, e.RowIndex);
                    ChangeCellStyle(e.ColumnIndex, e.RowIndex, _wallCellStyle);
                }                
            }
        }
        #endregion

        #region - cell style

        private void ChangeCellStyle(int x, int y, DataGridViewCellStyle style)
        {
            this.dGVBoard.Rows[y].Cells[x].Style = style;
        }

        private void ClearPath()
        {
            for (int y = 0; y < dGVBoard.Rows.Count; y++)
            {
                for (int x = 0; x < dGVBoard.Rows[y].Cells.Count; x++)
                {
                    if (dGVBoard.Rows[y].Cells[x].Style == _pathCellStyle)
                    {
                        ChangeCellStyle(x, y, null);
                    }
                }
            }
        }
        private void SetUpCellStyles()
        {
            this._startCellStyle = new DataGridViewCellStyle();
            this._startCellStyle.BackColor = Color.Orange;
            this._startCellStyle.SelectionBackColor = Color.Orange;
            this._targetCellStyle = new DataGridViewCellStyle();
            this._targetCellStyle.BackColor = Color.Blue;
            this._targetCellStyle.SelectionBackColor = Color.Blue;
            this._pathCellStyle = new DataGridViewCellStyle();
            this._pathCellStyle.BackColor = Color.Yellow;
            this._wallCellStyle = new DataGridViewCellStyle();
            this._wallCellStyle.BackColor = Color.Gray;
            this._wallCellStyle.SelectionBackColor = Color.Gray;
            this._wallCellStyle.ForeColor = Color.Gray;
            this._wallCellStyle.SelectionForeColor = Color.Gray;
        }
        #endregion

        private void CreateDataGrid()
        {
            string[] row = new string[xSize];
            IField[,] source = this._board.GetBoard();

            for (int i = 0; i < xSize; i++)
            {
                this.dGVBoard.Columns.Add(i.ToString(), i.ToString());
            }

            for (int y = 0; y < ySize; y++)
            {
                for (int x = 0; x < xSize; x++)
                {
                    row[x] = string.Format("Id: {0}, Cost: {1}", source[x, y].Id, source[x, y].FieldCost);
                }

                this.dGVBoard.Rows.Add(row);
                row = new string[xSize];
            }
        }

        private async void ExecuteAlrogithm()
        {
            ClearPath();

            Stack<IField> path = null;
            PathFindingBase pathFinder;
            IField pathField;
            Stopwatch sw = new Stopwatch();

            pathFinder = (PathFindingBase)CbAlgorithm.SelectedItem;
            sw.Start();

            try
            {
                pathFinder.UpdateBoard(_board);
                path = await pathFinder.GetPath(_startField, _targetField);

                while (path.Count > 1)
                {
                    pathField = path.Pop();
                    if (!(pathField.X == _startField.X &&
                        pathField.Y == _startField.Y))
                    {
                        ChangeCellStyle(pathField.X, pathField.Y, _pathCellStyle);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            sw.Stop();

            PbProgress.Style = ProgressBarStyle.Blocks;
            LaPathCost.Text = pathFinder.PathCost.ToString("#.#");
            LaPathLength.Text = pathFinder.PathLength.ToString();
            LaTime.Text = string.Format("{0} ms", sw.ElapsedMilliseconds);
        }
        #endregion

        #region - ctors
        public MainWindow()
        {
            InitializeComponent();

            this._board = new Board(xSize, ySize);
            this._board.CreateBoard();
            this.CreateDataGrid();
            this.SetUpCellStyles();
            CbAlgorithm.Items.Add(new Dijkstra(_board));
            CbAlgorithm.Items.Add(new AStar(_board, new DiagonalHeuristicCalculator()));
            CbAlgorithm.Items.Add(new AStar(_board, new ManhattanHeuristicCalculator()));
            CbAlgorithm.Items.Add(new BestFirstSearch(_board, new DiagonalHeuristicCalculator()));
            CbAlgorithm.Items.Add(new BestFirstSearch(_board, new ManhattanHeuristicCalculator()));
            CbAlgorithm.Items.Add(new BreadthFirstSearch(_board));
            CbAlgorithm.SelectedIndex = 0;
        }
        #endregion
        #endregion
    }
}

﻿namespace PathFinding.Enviroment
{
    public class Field : IField
    {
        #region - public
        #region - properties

        public int Id { get; set; }
        public int FieldCost { get; set; }
        public int X { get; private set; }
        public int Y { get; private set; }

        #endregion

        #region - ctors
        public Field(int id, int fieldCost, int x, int y)
        {
            this.Id = id;
            this.FieldCost = fieldCost;
            this.X = x;
            this.Y = y;
        }
        #endregion
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding.Enviroment
{
    public class Board : IBoard
    {
        #region - private
        #region - vars

        private IDictionary<int, Field> _boardDic;
        private Field[,] _board;
        private int _xSize;
        private int _ySize;
        private const int maxFieldCost = 15;
        private const int wallPathCost = int.MaxValue;

        #endregion

        #region - functions

        private IList<Field> GetXNeighbours(int yPos, int xStart)
        {
            IList<Field> result = new List<Field>();

            if (yPos >= 0 &&
                yPos < this._ySize)
            {
                for (int x = xStart - 1; x <= xStart + 1; x++)
                {
                    if (x >= 0 &&
                        x < this._xSize)
                    {
                        result.Add(this._board[x, yPos]);
                    }
                }
            }

            return result;
        }
        #endregion
        #endregion

        #region - public
        #region - functions

        public IField[,] GetBoard()
        {
            return this._board;
        }

        public void AddWall(int x, int y)
        {
            if ((x >= 0 &&
                x < GetXSize()) &&
                y >= 0 &&
                y < GetYSize())
            {
                _board[x, y].FieldCost = wallPathCost;
            }
        }

        public void RemoveWall(int x, int y)
        {
            if ((x >= 0 &&
                x < GetXSize()) &&
                y >= 0 &&
                y < GetYSize() &&
                IsWall(x, y))
            {
                _board[x, y].FieldCost = new Random().Next(maxFieldCost);
            }
        }

        public bool IsWall(int id)
        {
            if (_boardDic.ContainsKey(id))
            {
                return _boardDic[id].FieldCost == wallPathCost;
            }

            return false;
        }

        public bool IsWall(int x, int y)
        {
            IField tmp = GetField(x, y);

            if (tmp == null)
            {
                throw new NullReferenceException("Field doesnt exist");
            }

            return IsWall(tmp.Id);
        }

        public void CreateBoard()
        {
            Random rnd = new Random();
            int id = 0;
            this._boardDic.Clear();
            Field tmp;

            for (int y = 0; y < this._ySize; y++)
            {
                for (int x = 0; x < this._xSize; x++)
                {
                    tmp = new Field(id, rnd.Next(1, maxFieldCost), x, y);
                    this._boardDic.Add(id, tmp);
                    this._board[x, y] = tmp;
                    id++;
                }
            }
        }

        public IField GetField(int x, int y)
        {
            if (x >= 0 &&
                x < GetXSize() &&
                y >= 0 &&
                y < GetYSize())
            {
                return _board[x, y];
            }

            return null;
        }

        public IField GetField(int id)
        {
            if (_boardDic.ContainsKey(id))
            {
                return _boardDic[id];
            }

            return null;
        }

        public bool IsDiagonalNeighbour(IField field, IField neighbour)
        {
            return field.X != neighbour.X &&
                   field.Y != neighbour.Y;
        }

        public IList<IField> GetFieldNeighbours(int fieldId)
        {
            IEnumerable<IField> result = new List<IField>();
            if (this._boardDic.ContainsKey(fieldId))
            {
                Field field = this._boardDic[fieldId];

                for (int y = field.Y - 1; y <= field.Y + 1; y++)
                {
                    result = result.Concat(this.GetXNeighbours(y, field.X));
                }

                result = result.Where((w) => !(w.X == field.X &&
                                             w.Y == field.Y));
            }

            return result.ToList();
        }

        public int GetXSize()
        {
            return this._xSize;
        }

        public int GetYSize()
        {
            return this._ySize;
        }
        #endregion

        #region - ctors
        public Board(int xSize, int ySize)
        {
            this._xSize = xSize;
            this._ySize = ySize;
            this._boardDic = new Dictionary<int, Field>();
            this._board = new Field[xSize, ySize];
            CreateBoard();
        }
        #endregion
        #endregion
    }
}

﻿using PathFinding.Algorithm.Algorithms;
using PathFinding.Algorithm.Heuristic;

using PathFinding.Enviroment;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PathFinding.Shell
{
    class Program
    {
        static void Main(string[] args)
        {
            Execute();
        }

        private static async void Execute()
        {
            int xSize;
            int ySize;
            int xStartPosition;
            int yStartPosition;
            int xTargetPosition;
            int yTargetPosition;
            int input;
            Board board;
            IField startField;
            IField targetField;

            PathFindingBase pathFinder = null;

            Console.WriteLine("X size of the board:");
            xSize = int.Parse(Console.ReadLine());
            Console.WriteLine("Y size of board:");
            ySize = int.Parse(Console.ReadLine());
            Console.WriteLine("X start position:");
            xStartPosition = int.Parse(Console.ReadLine());
            Console.WriteLine("Y start position:");
            yStartPosition = int.Parse(Console.ReadLine());
            Console.WriteLine("X target position:");
            xTargetPosition = int.Parse(Console.ReadLine());
            Console.WriteLine("Y target position:");
            yTargetPosition = int.Parse(Console.ReadLine());

            board = new Board(xSize, ySize);

            while (true)
            {
                Console.WriteLine("(0) Exit (1) for Dijkstra (2) for A*, (3) Best First Search");
                input = int.Parse(Console.ReadLine());

                if (input == 0)
                {
                    break;          
                }
                else if (input == 1)
                {
                    pathFinder = new Dijkstra(board);                   
                }
                else if (input == 2)
                {
                    pathFinder = new AStar(board, new DiagonalHeuristicCalculator());
                }
                else if (input == 3)
                {
                    pathFinder = new BestFirstSearch(board, new DiagonalHeuristicCalculator());
                }

                startField = board.GetField(xStartPosition, yStartPosition);
                targetField = board.GetField(xTargetPosition, yTargetPosition);

                Stopwatch sw = new Stopwatch();
                sw.Start();
                Task<Stack<IField>> t = pathFinder.GetPath(startField, targetField);
                t.Wait();
                sw.Stop();
                Console.WriteLine("Execution time: {0} ms", sw.ElapsedMilliseconds.ToString());
                Console.WriteLine("Path length: {0}, Path cost: {1}", pathFinder.PathLength, pathFinder.PathCost);

            }
        }
    }
}
